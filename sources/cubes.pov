/*
// file: cube-joueur.pov
// date:
// auteur: pascal TOLEDO
// site: http://www.legral.fr
// description: cube avec texture individuel par face
*/

/********************************************************
	INCLUSION FICHIERS STANDART
********************************************************/
//Library_Path="Z:\mnt\sshfs\rps2-home\homePerso\pascal\images\povray"
//#include "./scene-standart.pov"

/* ==== divers ==== */
//#include "math.inc"		//---- general math functions and macros
//#include "rand.inc"	// random number generation macros
//#include "consts.inc"
//#include "arrays.inc"	// array manipulation macros
//#include "strings.inc"	// macros for generating and manipulating text strings
//#include "transforms.inc"	// various geometric transformation macros


/* ==== formes ==== */
//#include "shapes.inc"	// macros for generating various shapes
//#include "shapes2.inc"	// some not built in basic shapes


/* ==== textures ==== */
#include "colors.inc"	// Standard pre-defined colors

//#include "skies.inc"	// some predefined skies

//#include "golds.inc"	// several different gold colors, finishes and textures
//#include "stones.inc"	// T_Stone1 - T_Stone44
//#include "metals.inc"	// various metal colors, finishes and textures  brass, copper, chrome, silver
//#include "glass.inc"	// various glass finishes, colors and interiors
//#include "woods.inc"	// various (mostly layered) wood textures // T_Wood1 - T_Wood35

/* ==== finition ==== */
//#include "finish.inc"	// some standard finishes


/********************************************************
	INCLUSION FICHIERS PERSO
********************************************************/
#include "lib/perso/debuger/debuger-1.0/debuger-1.0.inc"
#include "lib/perso/povray-general/povray-general-0.1.inc"
                                       

/********************************************************
	PARAMETRES
********************************************************/
//#declare debuger_actif=1;

#declare vue_orientation=0;	// initialisation 

#declare imageNo=1;	// derriere le cube (corps) ...
#declare imageNo=2;	// il y a l'etre parfait... cercle
#declare imageNo=3;	// le joueur + cercle
#declare imageNo=4;	// le joueur + cercle + anim 360*y


/********************************************************
	PARAMETRES PAR DEFAUT DE LA SCENE
********************************************************/
#if( version < 3.7 ) global_settings{assumed_gamma 1.0} #end
#default
	{
	finish
		{
		conserve_energy			/* http://www.povray.org/documentation/view/3.6.1/349/ */

		ambient rgb <0.1,0.1,0.1>	// [0-1](0.1) lumiere dans les ombres /* http://www.povray.org/documentation/view/3.6.1/345/ */

		// Diffuse Reflection Items /* http://www.povray.org/documentation/view/3.6.1/346/ */
		diffuse 0.6				// [0-1](0.6)
		brilliance 1.0				// [0-x](1.0)
		crand 0.0 				//ne pas utiliser en animation
		}
	}
 /********************************************************
	REPERE
********************************************************/
//#include "../../../lib/perso/objets/delimitation/delimitation-1.0.inc"                                    
#include "lib/perso/objets/delimitation/delimitation-1.0.inc"                                    
                                  
                                  
//delimitation(long,larg,haut,rayon)
object {delimitation(4,4,4,0.01) texture{pigment{color White}} translate <-2,-2,-2>}
//AxisXYZ( 2, 2, 2, Texture_A_Dark, Texture_A_Light)

/********************************************************
	CAMERA
********************************************************/
#local camX=0;#local camY=7;#local camZ=13;
#if (imageNo = 1)#local camX=0;#local camY=7;#local camZ=13;#end
#if (imageNo = 2)#local camX=0;#local camY=7;#local camZ=13;#end
#if (imageNo=3)#local camX=0;#local camY=7;#local camZ=13;#end

#declare camera0=
camera {
	perspective
	location  <0,7, -camZ>
	look_at   <0,7, camZ >
	right     x*image_width/image_height  // aspect
	// direction z                        // direction and zoom
	// angle 67                           // field (overides direction zoom)
}

/********************************************************
	SOURCES DE LUMIERES
********************************************************/
#declare lumiere1=
light_source
	{
	<100, 100, -100>
	color White
	//rgb <0.1,0.2,0.7>
	parallel
	point_at <0, 0, 0>
	}
	
/********************************************************
	ACTIVATION-ORIENTATION CAMERA ET SOURCES DE LUMIERES
********************************************************/
camera{camera0}
light_source{lumiere1}

#switch (vue_orientation)  
	#case (1)
		//camera {camera0 rotate 0*y}
		//light_source{lumiere1}
		#break; 
	//#case (7)	camera {camera0 rotate 180*y} #break; 
	//#range(2,10)  sphere{<0,2,0>,2 pigment{ Green }}     #break; 
	//#else         sphere{<0,2,0>,2 pigment{ Brown }}     #break; 
#end



/********************************************************
	CALCUL DU NUMERO DE PHASE_TIME
********************************************************/

#declare phaseNo=1;
#if (clock<1)			#declare phaseNo=1;#end
//#if (clock>1 & clock=<1.1)	#declare phaseNo=2.1;	#end
#if (clock>=1 & clock<2)	#declare phaseNo=2;		#end

#if (clock>=2 & clock<3)	#declare phaseNo=3;		#end
#if (clock>=3 & clock<4)	#declare phaseNo=4;		#end

#declare phase_clock=clock-phaseNo+1;	


/********************************************************
	OBJET: le cube_joueur
********************************************************/

/*======================================================*/
#declare cube0 =
	box	{
		<0.1,0.1,0.1>  // one corner position <X1 Y1 Z1>
		<0.9,0.9,0.9>  // other corner position <X2 Y2 Z2>
		}
/*======================================================*/
#declare tete =
cylinder{0*z,  1*z,  0.9/2
	texture{pigment{color Yellow}}
	}

/*======================================================*/
#declare corps =
	box	{
		<0.1,0.1,0.1>
		<2.5,1.9,0.9>
		texture{
			pigment{color Blue}
			}
		}

/*======================================================*/
#declare fesses =
	box	{
		<0.1,0.1,0.1>
		<2.5,0.9,0.9>
		texture{
			pigment{color Green}
			}
		}

/*======================================================*/
//jambe gauche
#declare jambe_gauche = union
	{
	object{cube0 translate 1*y}
	object{cube0 translate 2*y}
	object{cube0 translate 3*y}
	object{cube0 translate 4*y}
	object{cube0 translate 5*y}
	texture{pigment{color Cyan}}
	}

//jambe droite
#declare jambe_droite = union
	{
	object{cube0 translate 1*y}
	object{cube0 translate 2*y}
	object{cube0 translate 3*y}
	object{cube0 translate 4*y}
	object{cube0 translate 5*y}
	translate 1.5*x
	texture{pigment{color Cyan}}
	}

/*======================================================*/
//bras gauche
#declare bras_gauche = union
	{
	object{cube0 translate 1*y}
	object{cube0 translate 2*y}
	object{cube0 translate 3*y}
	object{cube0 translate 4*y}
	translate -0.5*x
	texture{pigment{color Green}}
	}

//bras droit
#declare bras_droit = union
	{
	object{cube0 translate 1*y}
	object{cube0 translate 2*y}
	object{cube0 translate 3*y}
	object{cube0 translate 4*y}
	translate -0.5*x
	texture{pigment{color Green}}
	}


/*==== dossard =========================================*/
#declare dossard = union
	{
	cylinder{<0,0,0.5><0,0,1>,0.8}
	texture{pigment{color Yellow}}
	}


/********************************************************
	AFFICHAGE
********************************************************/


object{corps  translate <0,007,0>translate -1.25*x}


#if (imageNo >1)
union
	{	// tout

	#if (imageNo >=3)

	union	//personnage
		{
		union
			{ 
			object{tete translate <1.25,10,0>}
			object{corps  translate <0,007,0>}
			object{fesses translate <0,006,0>}

			object{jambe_gauche}
			object{jambe_droite}
			translate -1.25*x
			}

		object {bras_gauche translate 0*y rotate +60*z translate <-0.5,8,0>}       
		object {bras_droit  translate 0*y rotate -60*z translate <+0.5,8,0>}       

		/*
		#local brasNu=0;
		#while (brasNu <12 )
		object {bras_gauche translate 0*y rotate 030*brasNu*z translate 0*y}       
		#local brasNu=brasNu+1;
		#end
		*/
          
		object{dossard translate <0,8,0>}
		}//#personnage
	#end          


	/********************************************************
	Cercle de la perfection
	********************************************************/
	object{
		torus {  6,  0.25}          
		rotate 90*x
		translate <0,7,0.3>
		texture
			{
			pigment{color Yellow}
			finish
				{
				phong 1.0
				}
			}
		}

	//rotate 90*y

	}	// # tout
#end          

	
	
//object{cube0 texture{pigment{color Yellow}}}          
/*       
texture
	{
	}
*/